<?php

Route::domain('api.hashtag.localhost')->group(function () {

    Route::group(['prefix' => '/v1', 'middleware' => 'auth', 'namespace' => 'Api\V1',
        'as' => 'gateway.v1.'], function () {

        Route::group(['prefix' => '/hashtags', 'namespace' => 'Hashtag', 'as' => 'hashtag.'], function () {

            Route::post('/', "HashtagController@create")->name('create');
            Route::get('/', "HashtagController@hashtags")->name('hashtags');

        });
    });

});
