<?php


namespace App\Http\Classes;

use App\Http\Interfaces\ResponderInterface;
use Illuminate\Http\Response;

class JsonResponder implements ResponderInterface
{

    /**
     * create " entity is created" response .
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function created()
    {
        return response()->json(['hashtag is created'], Response::HTTP_CREATED);
    }
}