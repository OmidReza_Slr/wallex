<?php

namespace App\Http\Repositories\hashtag;


interface HashtagRepositoryInterface
{
    /**
     * create new hashtag.
     *
     * @param array $data that received from api.feed.localhost.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($data);

}