<?php

namespace App\Http\Repositories\hashtag;


use App\Hashtag;
use App\Http\Interfaces\RequestSenderInterface;
use App\Http\Interfaces\ResponderInterface;
use Illuminate\Support\Facades\DB;

class EloquentHashtagRepository implements HashtagRepositoryInterface
{
    /**
     * @var Hashtag $hashtag
     */
    private $hashtag;

    /**
     * @var RequestSenderInterface $requestSender
     */
    private $requestSender;
    /**
     * @var ResponderInterface
     */
    private $responder;

    /**
     * EloquentFeedRepository constructor.
     * @param Hashtag $hashtag
     * @param RequestSenderInterface $requestSender
     * @param ResponderInterface $responder
     */
    public function __construct(Hashtag $hashtag, RequestSenderInterface $requestSender, ResponderInterface $responder)
    {
        $this->feed = $hashtag;
        $this->requestSender = $requestSender;
        $this->responder = $responder;
    }

    /**
     * create new feed with or without own hashTags.
     *
     * @param array $data that send from api.gateway.localhost.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($data)
    {
        $insertToPivotTables = [];

        foreach ($data['hashtags'] as $key => $hashtag) {
            $hashtagCreate = $this->hashtag->create($hashtag);
            $insertToPivotTables[$key] = ['hashtag_id' => $hashtagCreate->id, 'feed_id' => $data['feed_id']];

        }

        DB::table('feed_hashtag')->insert($insertToPivotTables);

        return $this->responder->created();
    }
}