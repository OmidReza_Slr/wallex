<?php

namespace App\Http\Interfaces;


interface RequestSenderInterface
{

    /**
     * Send information along with information to the destination
     *
     * @param array $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(array $data);

}