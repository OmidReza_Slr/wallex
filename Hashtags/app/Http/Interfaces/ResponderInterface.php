<?php

namespace App\Http\Interfaces;


interface ResponderInterface
{
    /**
     * create " entity is created" response .
     *
    @return \Illuminate\Http\JsonResponse
     */
    public function created();

}