<?php

namespace App\Http\Controllers\Api\V1\Hashtag;

use App\Http\Controllers\Controller;
use App\Http\Repositories\hashtag\HashtagRepositoryInterface;
use Illuminate\Http\Request;

class HashtagController extends Controller
{
    /**
     * @var HashtagRepositoryInterface $repository
     */
    private $repository;

    /**
     * @param HashtagRepositoryInterface $repository
     */
    public function construct(HashtagRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * create new feed with or without own hashTags.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function create(Request $request)
    {
        $this->repository->create($request->all());

    }
}
