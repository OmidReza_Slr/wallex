<?php

namespace App\Providers;

use App\Http\Classes\GuzzleHttpSender;
use App\Http\Classes\JsonResponder;
use App\Http\Interfaces\RequestSenderInterface;
use App\Http\Interfaces\ResponderInterface;
use App\Http\Repositories\hashtag\EloquentHashtagRepository;
use App\Http\Repositories\hashtag\HashtagRepositoryInterface;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->contracts();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * connect abstraction class and interface with target their target classes.
     *
     * @return void
     */

    private function contracts()
    {
        $this->app->singleton(RequestSenderInterface::class, GuzzleHttpSender::class);
        $this->app->singleton(ResponderInterface::class, JsonResponder::class);
        $this->app->singleton(HashtagRepositoryInterface::class, EloquentHashtagRepository::class);


    }
}
