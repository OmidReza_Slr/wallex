<?php

namespace App\Http\Interfaces;


interface ResponderInterface
{
    /**
     * create " entity is created" response .
     *
     * * @param array $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($data);

}