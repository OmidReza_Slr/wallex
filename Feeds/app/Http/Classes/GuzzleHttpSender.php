<?php

namespace App\Http\Classes;


use App\Http\Interfaces\RequestSenderInterface;
use Illuminate\Support\Facades\Http;

class GuzzleHttpSender implements RequestSenderInterface
{

    /**
     * Send information along with information to the destination
     *
     * @param array $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(array $data)
    {
        $response = Http::withHeaders(['Accept' => 'application/json'])
            ->post('api.hashtag.localhost/hashtags', $data)->json();
        return $response;
    }
}