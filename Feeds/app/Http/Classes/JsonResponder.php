<?php


namespace App\Http\Classes;

use App\Http\Interfaces\ResponderInterface;
use Illuminate\Http\Response;

class JsonResponder implements ResponderInterface
{

    /**
     * create " entity is created" response .
     *
     * * @param array $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($data)
    {
        // TODO: Implement create() method.
    }
}