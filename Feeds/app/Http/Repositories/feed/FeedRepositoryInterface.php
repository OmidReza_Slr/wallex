<?php

namespace App\Http\Repositories\feed;


interface FeedRepositoryInterface
{
    /**
     * create new feed with or without own hashTags.
     *
     * @param array $data that send from api.gateway.localhost.
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($data);

}