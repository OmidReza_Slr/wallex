<?php
/**
 * Created by PhpStorm.
 * User: Omid-PC
 * Date: 8/15/2020
 * Time: 7:01 PM
 */

namespace App\Http\Repositories\feed;


use App\Feed;
use App\Http\Interfaces\RequestSenderInterface;
use App\Http\Interfaces\ResponderInterface;

class EloquentFeedRepository implements FeedRepositoryInterface
{
    /**
     * @var Feed $feed
     */
    private $feed;

    /**
     * @var RequestSenderInterface $requestSender
     */
    private $requestSender;
    /**
     * @var ResponderInterface
     */
    private $responder;

    /**
     * EloquentFeedRepository constructor.
     * @param Feed $feed
     * @param RequestSenderInterface $requestSender
     * @param ResponderInterface $responder
     */
    public function __construct(Feed $feed, RequestSenderInterface $requestSender, ResponderInterface $responder)
    {
        $this->feed = $feed;
        $this->requestSender = $requestSender;
        $this->responder = $responder;
    }

    /**
     * create new feed with or without own hashTags.
     *
     * @param array $data that send from api.gateway.localhost.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($data)
    {

        $feed = $this->feed->create($data);

        $data['feed_id'] = $feed->id;

        $response = $this->requestSender->send($data);

        return $this->responder->create($response);


    }
}