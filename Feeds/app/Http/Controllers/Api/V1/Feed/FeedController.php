<?php

namespace App\Http\Controllers\Api\V1\Feed;

use App\Http\Controllers\Controller;
use App\Http\Repositories\feed\FeedRepositoryInterface;
use Illuminate\Http\Request;

class FeedController extends Controller
{
    /**
     * @var FeedRepositoryInterface $repository
     */
    private $repository;

    /**
     * @param FeedRepositoryInterface $repository
     */
    public function construct(FeedRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * create new feed with or without own hashTags.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function create(Request $request)
    {
        $this->repository->create($request->all());

    }
}
