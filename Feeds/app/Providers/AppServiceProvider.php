<?php

namespace App\Providers;

use App\Http\Classes\GuzzleHttpSender;
use App\Http\Classes\JsonResponder;
use App\Http\Interfaces\RequestSenderInterface;
use App\Http\Interfaces\ResponderInterface;
use App\Http\Repositories\feed\EloquentFeedRepository;
use App\Http\Repositories\feed\FeedRepositoryInterface;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->contracts();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * connect abstraction class and interface with target their target classes.
     *
     * @return void
     */

    private function contracts()
    {
        $this->app->bind(FeedRepositoryInterface::class, EloquentFeedRepository::class);
        $this->app->singleton(RequestSenderInterface::class, GuzzleHttpSender::class);
        $this->app->singleton(ResponderInterface::class, JsonResponder::class);


    }
}
