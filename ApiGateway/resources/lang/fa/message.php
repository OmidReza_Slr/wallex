<?php

return [
    'ticket-created' => 'تیکت شما با موفقیت ثبت شد ، کد پیگیری شما : :code',
    'ticket-not-found' => "تیکتی با این مشخصات یافت نشد.",
    'Unauthorized' => 'شما وارد نشدید',
    'AccessForbidden' => "شما اجازه دسترسی به این بخش را ندارید.",
    'roleCreated' => "نقش جدید با موفقیت انجام شد.",
    'permissionCreated' => "دسترسی جدید با موفقیت انجام شد.",
];
