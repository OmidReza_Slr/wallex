<?php


namespace App\Http\Classes;


class EloquentAuth
{

    /**
     * get online user
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function auth()
    {
        return aurh()->user();
    }

    /**
     * get online user id
     *
     * @return int
     */

    public function onlineUserId()
    {
        return auth()->user()->id;
    }

}