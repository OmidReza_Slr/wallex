<?php

namespace App\Http\Requests\Api\V1\Feed;

use Illuminate\Foundation\Http\FormRequest;

class CreateFeedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => "required|string|max:85",
            'description' => "required",
            'hashtags' => 'nullable|array',
            'hashtags.*.title' => 'nullable|string'
        ];
    }

    public function all($keys = null)
    {
        $request = parent::all();

        $request['hashtags'] = isset($request['hashtags']) ? json_decode($request['hashtags'], true) : [];

        return $request;

    }
}
