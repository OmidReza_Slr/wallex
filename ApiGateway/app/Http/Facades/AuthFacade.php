<?php


namespace App\Http\Facades;

use Illuminate\Support\Facades\Facade;

class AuthFacade extends Facade
{

    /**
     * @method array auth()
     * @method array onlineUserId()
     */

    /**
     * @inheritdoc
     *
     */
    protected static function getFacadeAccessor()
    {
        return __CLASS__;
    }

    /**
     * Communicates the class with your contract
     *
     * @param object $class
     *
     * @return void
     */

    static function shouldProxyTo($class)
    {
        app()->singleton(self::getFacadeAccessor(), $class);
    }

}