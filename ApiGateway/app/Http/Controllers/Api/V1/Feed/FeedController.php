<?php

namespace App\Http\Controllers\Api\V1\Feed;


use App\Http\Controllers\Controller;
use App\Http\Facades\AuthFacade;
use App\Http\Interfaces\RequestSenderInterface;
use App\Http\Requests\Api\V1\Feed\CreateFeedRequest;

class FeedController extends Controller
{

    /**
     * @var RequestSenderInterface
     */
    private $requestSender;

    /**
     * FeedController constructor.
     * @param RequestSenderInterface $requestSender
     */

    public function __construct(RequestSenderInterface $requestSender)
    {
        $this->requestSender = $requestSender;
    }


    /**
     * make valid request and send to their microservice.
     *
     * @param CreateFeedRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CreateFeedRequest $request)
    {
        $userId = AuthFacade::onlineUserId();

        $request['user_id'] = $userId;

        $response = $this->requestSender->send($request->all());

        return $response;

    }
}
