<?php

namespace App\Providers;

use App\Http\Classes\EloquentAuth;
use App\Http\Classes\GuzzleHttpSender;
use App\Http\Facades\AuthFacade;
use App\Http\Interfaces\RequestSenderInterface;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->contracts();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * connect abstraction class and interface with target their target classes.
     *
     * @return void
     */

    private function contracts()
    {
        AuthFacade::shouldProxyTo(EloquentAuth::class);
        $this->app->singleton(RequestSenderInterface::class, GuzzleHttpSender::class);


    }
}
