<?php


namespace Login\Contracts;


interface LoginInterface
{
    /**
     * Allows access to log in to received token.
     *
     * $requests has its own $data array, whose ['args'] key
     *           should contain a JSON value.  In the JSON, keys represent
     *           the fields to login.
     *
     * @return string|null
     */

    public function login($request);

}
