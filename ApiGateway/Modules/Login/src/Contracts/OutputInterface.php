<?php

namespace Login\Contracts;


interface OutputInterface
{

    /**
     * when the user's authentication is not confirmed.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function unauthorized();

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function respondWithToken($token);

    /**
     * Choose which response to send user after user request for login.
     *
     * @param  string|null $token is string or null.
     *
     * @return boolean
     */
    public function chooseResponseLogin($token);

    /**
     * Send user information that is in the online system.
     *
     * @param object $user A user who is online.
     *
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function loggedInUserInfo($user);
}
