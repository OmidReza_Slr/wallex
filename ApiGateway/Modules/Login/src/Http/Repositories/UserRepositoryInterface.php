<?php

namespace Login\Http\Repositories;

interface UserRepositoryInterface
{
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function onlineUser();


    /**
     * create new  or update user.
     *
     * @param array $data
     *
     * @return boolean
     */
    public function createOrUpdate(array $data);


}