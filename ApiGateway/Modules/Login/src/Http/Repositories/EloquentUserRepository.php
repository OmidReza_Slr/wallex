<?php

namespace Login\Http\Repositories;


use Login\Entities\User;

class EloquentUserRepository implements UserRepositoryInterface
{
    /**
     * @var User $user
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function onlineUser()
    {
        return auth()->user();
    }

    /**
     * create new  or update user.
     *
     * @param array $data
     *
     * @return boolean
     */
    public function createOrUpdate(array $data)
    {
        $this->user->updateOrCreate(['email' => $data['email']], $data);

        return true;

    }
}