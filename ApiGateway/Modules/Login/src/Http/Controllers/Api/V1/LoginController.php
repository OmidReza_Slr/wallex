<?php

namespace Login\Http\Controllers\Api\V1;

use Illuminate\Routing\Controller;
use Login\Contracts\LoginInterface;
use Login\Contracts\OutputInterface;
use Login\Http\Repositories\UserRepositoryInterface;
use Login\Http\Requests\LoginRequest;

class LoginController extends Controller
{

    /**
     * @var LoginInterface
     */
    private $login;

    /**
     * @var OutputInterface
     */
    private $output;
    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * AuthController constructor.
     * @param LoginInterface $login
     * @param OutputInterface $output
     * @param UserRepositoryInterface $repository
     */

    public function __construct(LoginInterface $login, OutputInterface $output,UserRepositoryInterface $repository)
    {
        $this->login = $login;
        $this->output = $output;
        $this->repository = $repository;
    }

    /**
     * login to application for get access token
     *
     * @param LoginRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function login(LoginRequest $request)
    {
        $token = $this->login->login($request->all());

        return $this->output->chooseResponseLogin($token);
    }



    public function LoggedInUserInfo()
    {
        $user=$this->repository->onlineUser();

       return $this->output->LoggedInUserInfo($user);

    }

}
