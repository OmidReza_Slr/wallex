<?php

namespace Login;

use DatabaseSeeder;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

use Login\Classes\Login\JwTLogin;
use Login\Classes\Outputs\JsonOutput;
use Login\Contracts\LoginInterface;
use Login\Contracts\OutputInterface;
use Login\database\seeders\UserTableSeeder;
use Login\Http\Repositories\EloquentUserRepository;
use Login\Http\Repositories\UserRepositoryInterface;

class LoginServiceProvider extends ServiceProvider
{
    private $namespace = 'Login\Http\Controllers';

    private $modulePath = '/Modules/Login/src';

    /**
     * @inheritdoc
     */

    public function register()
    {
        DatabaseSeeder::$seeders[] = UserTableSeeder::class;

        $this->bindContracts();
    }

    /**
     * @inheritdoc
     */
    public function boot()
    {
        $this->defineRoutes();
        $this->loadFactoriesFrom($this->modulePath . '/database/factories');
    }

    /**
     * Register  package routes.
     *
     * @return void
     */

    private function defineRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(__DIR__ . './login_routes.php');
    }

    /**
     * Register Interfaces for usage.
     *
     * @return void
     */

    private function bindContracts()
    {
        $this->app->bind(LoginInterface::class, JwTLogin::class);
        $this->app->bind(OutputInterface::class, JsonOutput::class);
        $this->app->bind(UserRepositoryInterface::class, EloquentUserRepository::class);
    }
}