<?php

Route::domain('api.gateway.localhost')->group(function () {

    Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'gateway.v1.'], function () {
        Route::post('/login', 'LoginController@login')->name('login');
        Route::get('/user', 'LoginController@LoggedInUserInfo')->name('online-user');


    });

});