<?php


namespace Login\Classes\Login;


use Login\Contracts\LoginInterface;

class JwTLogin implements LoginInterface
{

    /**
     * Allows access to log in to received token.
     *
     * $requests has its own $data array, whose ['args'] key
     *           should contain a JSON value.  In the JSON, keys represent
     *           the fields to login.
     *
     * @return string|null
     */
    public function login($request)
    {
        $credentials = request(['email', 'password']);

        return $token = auth()->attempt($credentials);
    }
}
