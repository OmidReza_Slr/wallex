<?php


namespace Login\Classes\Outputs;


use Illuminate\Http\Response;
use Login\Contracts\OutputInterface;

class JsonOutput implements OutputInterface
{

    /**
     * when the user's authentication is not confirmed.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function unauthorized()
    {
        return response()->json([__('message.Unauthorized')], Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    /**
     * Choose which response to send user after user request for login.
     *
     * @param  string|null $token is string or null.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function chooseResponseLogin($token)
    {
        return !$token ? $this->unauthorized() : $this->respondWithToken($token);
    }

    /**
     * Send user information that is in the online system.
     *
     * @param object $user A user who is online.
     *
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function loggedInUserInfo($user)
    {
        return response()->json($user);
    }
}
