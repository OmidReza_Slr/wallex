<?php

Route::domain('api.gateway.localhost')->group(function () {

    Route::group(['prefix' => '/v1', 'middleware' => 'auth', 'namespace' => 'Api\V1',
        'as' => 'gateway.v1.'], function () {

        Route::group(['prefix' => '/feeds', 'namespace' => 'Feed', 'as' => 'feed.'], function () {

            Route::post('/', "FeedController@create")->name('create');
            Route::get('/', "FeedController@feeds")->name('feeds');

        });

    });

});
